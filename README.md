# DTESV-Signer
This replaces the Java project provided by Ministerio de Hacienda to sign DTEs.

Place your certificate in a folder called `uploads` in the root of this project:

```
<root>/uploads/Certificado_[nit].crt
```
