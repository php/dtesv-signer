<?php

namespace Datolab\DtesvSigner\models;

abstract class Documents
{
    public string $_id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->_id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->_id = $id;
    }

}
