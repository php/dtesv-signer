<?php

namespace Datolab\DtesvSigner\models;

use Datolab\DtesvSigner\Constants\KeyType;

require_once __DIR__.'/../constants/KeyType.php';

class Key
{
    private KeyType $keyType;
    private string $algorithm;
    private string $encoded;
    private string $format;
    private string $clave;


	public function __construct($keyType, $algorithm, $encoded, $format, $clave)
	{
		$this->setKeyType(new KeyType($keyType));
		$this->setAlgorithm($algorithm);
		$this->setEncoded($encoded);
		$this->setFormat($format);
		$this->setClave($clave);

	}

    /**
     * @return KeyType
     */
    public function getKeyType(): KeyType
    {
        return $this->keyType;
    }

    /**
     * @param KeyType $keyType
     */
    public function setKeyType(KeyType $keyType): void
    {
        $this->keyType = $keyType;
    }

    /**
     * @return string
     */
    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    /**
     * @param string $algorithm
     */
    public function setAlgorithm(string $algorithm): void
    {
        $this->algorithm = $algorithm;
    }

    /**
     * @return string
	 */
    public function getEncoded(): string
    {
        return $this->encoded;
    }

	/**
	 * @param string $encoded
	 */
    public function setEncoded(string $encoded): void
    {
        $this->encoded = $encoded;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat(string $format): void
    {
        $this->format = $format;
    }

    /**
     * @return string
     */
    public function getClave(): string
    {
        return $this->clave;
    }

    /**
     * @param string $clave
     */
    public function setClave(string $clave): void
    {
        $this->clave = $clave;
    }

	public function getFormatted($type = 'PRIVATE'): string
	{
		// TODO:HC Implement other formats!
		$encoded = $this->getEncoded();
		return 	<<<EOD
-----BEGIN ${type} KEY-----
${encoded}
-----END ${type} KEY-----
EOD;
	}

    public function toString(): string {
        return "Key [keyType=" . $this->getKeyType()->getType() . ", algorithm=" . $this->getAlgorithm() . ", encodied=" . $this->getEncoded()
            . ", format=" . $this->getFormat() . "]";
    }
}
