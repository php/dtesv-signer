<?php

namespace Datolab\DtesvSigner\models;

use Datolab\DtesvSigner\Business\Certificate;
require_once 'Documents.php';
class MHCertificate extends Documents
{
    private string $nit;
    private Key $publicKey;
    private Key $privateKey;
    private bool $active;
    private Certificate $certificate;
    private string $clavePub;
    private string $clavePri;

    /**
     * @return string
     */
    public function getNit(): string
    {
        return $this->nit;
    }

    /**
     * @param string $nit
     */
    public function setNit(string $nit): void
    {
        $this->nit = $nit;
    }

    /**
     * @return String
     */
    public function getClavePub(): string
    {
        return $this->clavePub;
    }

    /**
     * @param String $clavePub
     */
    public function setClavePub(string $clavePub): void
    {
        $this->clavePub = $clavePub;
    }

    /**
     * @return String
     */
    public function getClavePri(): string
    {
        return $this->clavePri;
    }

    /**
     * @param String $clavePri
     */
    public function setClavePri(string $clavePri): void
    {
        $this->clavePri = $clavePri;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }
    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return Key
     */
    public function getPublicKey(): Key
    {
        return $this->publicKey;
    }

    /**
     * 	@param Key $publicKey
	 */
    public function setPublicKey(Key $publicKey): void
    {
        $this->publicKey = $publicKey;
    }

    /**
     * @return Certificate
     */
    public function getCertificate(): Certificate
    {
        return $this->certificate;
    }

    /**
     * @param Certificate $certificate
     */
    public function setCertificate(Certificate $certificate): void
    {
        $this->certificate = $certificate;
    }



    /**
     * @return Key
     */
    public function getPrivateKey(): Key
    {
        return $this->privateKey;
    }

    /**
     * @param Key $privateKey
     */
    public function setPrivateKey(Key $privateKey): void
    {
        $this->privateKey = $privateKey;
    }

    public function toString(): string
    {
        return "Certificado [nit=" . $this->nit .
               ", publicKey=" . $this->publicKey->toString() .
               ", privateKey=" . $this->privateKey->toString() .
               ", activo=" . $this->active .
               ", clavePub=" . $this->clavePub .
               ", clavePri=" . $this->clavePri . "]";
    }
}
