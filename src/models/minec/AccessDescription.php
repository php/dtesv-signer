<?php

namespace Datolab\DtesvSigner\models\minec;

class AccessDescription
{
    private string $accessMethod;
    private array $accessLocation;

    public function __construct($accessMethod = "", $accessLocation = null)
    {
        $this->accessMethod = $accessMethod;
        $this->accessLocation = (array)$accessLocation ?? [];
    }

    public function addAccessLocation($accessLocation): void
    {
        $this->accessLocation[] = $accessLocation;
    }

    public function getAccessMethod(): string
    {
        return $this->accessMethod;
    }

    public function setAccessMethod($accessMethod): void
    {
        $this->accessMethod = $accessMethod;
    }

    public function getAccessLocation(): array
    {
        return $this->accessLocation;
    }

    public function setAccessLocation($accessLocation): void
    {
        $this->accessLocation = $accessLocation;
    }
}
