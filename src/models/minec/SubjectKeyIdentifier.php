<?php

namespace Datolab\DtesvSigner\models\minec;

class SubjectKeyIdentifier
{
    private string $keyIdentifier;

	public function __construct($keyIdentifier)
	{
		$this->setKeyIdentifier($keyIdentifier);
	}

	/**
     * @return string
     */
    public function getKeyIdentifier(): string
    {
        return $this->keyIdentifier;
    }

    /**
     * @param string $keyIdentifier
     */
    public function setKeyIdentifier(string $keyIdentifier): void
    {
        $this->keyIdentifier = $keyIdentifier;
    }
}
