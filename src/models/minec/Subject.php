<?php

namespace Datolab\DtesvSigner\models\minec;

class Subject
{
    private string $countryName;

    private string $organizationName;

    private string $organizationUnitName;

    private string $organizationIdentifier;

    private string $surname;

    private string $givenName;

    private string $commonName;

    private string $description;

    public function __construct(
        string $organizationName,
        string $organizationUnitName,
        string $organizationIdentifier,
        string $surname,
        string $givenName,
        string $commonName,
        string $description,
        string $countryName = 'EL SALVADOR')
    {
        $this->countryName = $countryName;
        $this->organizationName = $organizationName;
        $this->organizationUnitName = $organizationUnitName;
        $this->organizationIdentifier = $organizationIdentifier;
        $this->surname = $surname;
        $this->givenName = $givenName;
        $this->commonName = $commonName;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getCountryName(): string
    {
        return $this->countryName;
    }

    /**
     * @param string $countryName
     */
    public function setCountryName(string $countryName): void
    {
        $this->countryName = $countryName;
    }

    /**
     * @return string
     */
    public function getOrganizationName(): string
    {
        return $this->organizationName;
    }

    /**
     * @param string $organizationName
     */
    public function setOrganizationName(string $organizationName): void
    {
        $this->organizationName = $organizationName;
    }

    /**
     * @return string
     */
    public function getOrganizationUnitName(): string
    {
        return $this->organizationUnitName;
    }

    /**
     * @param string $organizationUnitName
     */
    public function setOrganizationUnitName(string $organizationUnitName): void
    {
        $this->organizationUnitName = $organizationUnitName;
    }

    /**
     * @return string
     */
    public function getOrganizationIdentifier(): string
    {
        return $this->organizationIdentifier;
    }

    /**
     * @param string $organizationIdentifier
     */
    public function setOrganizationIdentifier(string $organizationIdentifier): void
    {
        $this->organizationIdentifier = $organizationIdentifier;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getGivenName(): string
    {
        return $this->givenName;
    }

    /**
     * @param string $givenName
     */
    public function setGivenName(string $givenName): void
    {
        $this->givenName = $givenName;
    }

    /**
     * @return string
     */
    public function getCommonName(): string
    {
        return $this->commonName;
    }

    /**
     * @param string $commonName
     */
    public function setCommonName(string $commonName): void
    {
        $this->commonName = $commonName;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}