<?php

namespace Datolab\DtesvSigner\models\minec;

class AlgorithmIdentifier
{
    private string $algorithm;
    private string $parameters;

    public function __construct($algorithm = "RSA", $parameters = null)
    {
        $this->algorithm = $algorithm;
        $this->parameters = $parameters;
    }

    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    public function setAlgorithm($algorithm): void
    {
        $this->algorithm = $algorithm;
    }

    public function getParameters(): string
    {
        return $this->parameters;
    }

    public function setParameters($parameters): void
    {
        $this->parameters = $parameters;
    }
}