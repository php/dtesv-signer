<?php

namespace Datolab\DtesvSigner\models\minec;

class Issuer
{
    private string $countryName;
    private string $localityName;
    private string $organizationalUnit;
    private string $organizationalName;
    private string $commonName;
    private string $organizationIdentifier;

    public function __construct(
        $countryName = "SV",
        $localityName = "SAN SALVADOR",
        $organizationalUnit = "MINISTERIO DE HACIENDA",
        $organizationalName = "DIRECCIÓN GENERAL DE IMPUESTOS INTERNOS",
        $commonName = "UNIDAD COORDINADORA DEL PROGAMA FORTALECIMIENTO A LA ADMINISTRACIÓN TRIBUTARÍA",
        $organizationIdentifier = "VATSV-0614-010111-003-2"
    )
    {
        $this->countryName = $countryName;
        $this->localityName = $localityName;
        $this->organizationalUnit = $organizationalUnit;
        $this->organizationalName = $organizationalName;
        $this->commonName = $commonName;
        $this->organizationIdentifier = $organizationIdentifier;
    }

    public function getCountryName(): string
    {
        return $this->countryName;
    }

    public function setCountryName($countryName): void
    {
        $this->countryName = $countryName;
    }

    public function getLocalityName(): string
    {
        return $this->localityName;
    }

    public function setLocalityName($localityName): void
    {
        $this->localityName = $localityName;
    }

    public function getOrganizationalUnit(): string
    {
        return $this->organizationalUnit;
    }

    public function setOrganizationalUnit($organizationalUnit): void
    {
        $this->organizationalUnit = $organizationalUnit;
    }

    public function getOrganizationalName(): string
    {
        return $this->organizationalName;
    }

    public function setOrganizationalName($organizationalName): void
    {
        $this->organizationalName = $organizationalName;
    }

    public function getCommonName(): string
    {
        return $this->commonName;
    }

    public function setCommonName($commonName): void
    {
        $this->commonName = $commonName;
    }

    public function getOrganizationIdentifier(): string
    {
        return $this->organizationIdentifier;
    }

    public function setOrganizationIdentifier($organizationIdentifier): void
    {
        $this->organizationIdentifier = $organizationIdentifier;
    }
}