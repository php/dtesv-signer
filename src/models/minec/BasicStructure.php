<?php

namespace Datolab\DtesvSigner\models\minec;

class BasicStructure
{
    private string $version;
    private string $serial;
    private SignatureAlgorithm $signatureAlgorithm;
    private Issuer $issuer;
    private Validity $validity;
    private Subject $subject;
    private SubjectPublicKeyInfo $subjectPublicKeyInfo;

    public function __construct($subject = null, $key = null, $version = null, $serial = null, $signatureAlgorithm = null, $issuer = null, $validity = null, $subjectPublicKeyInfo = null) {
        if ($subject !== null && $key !== null) {
            $this->version = "2";
            $this->serial = "1.2.840.113549.1.1.11";
            $this->signatureAlgorithm = new SignatureAlgorithm();
            $this->issuer = new Issuer();
            $this->validity = new Validity();
            $this->subject = $subject;
            $this->subjectPublicKeyInfo = new SubjectPublicKeyInfo(
                new AlgorithmIdentifier($key->getAlgorithm(), null),
                $key->getEncodied()
            );
        } elseif ($version !== null && $serial !== null && $signatureAlgorithm !== null && $issuer !== null && $validity !== null && $subject !== null && $subjectPublicKeyInfo !== null) {
            $this->version = $version;
            $this->serial = $serial;
            $this->signatureAlgorithm = $signatureAlgorithm;
            $this->issuer = $issuer;
            $this->validity = $validity;
            $this->subject = $subject;
            $this->subjectPublicKeyInfo = $subjectPublicKeyInfo;
        }
    }

    public function getVersion(): string {
        return $this->version;
    }

    public function setVersion($version): void {
        $this->version = $version;
    }

    public function getSerial(): string {
        return $this->serial;
    }

    public function setSerial($serial): void {
        $this->serial = $serial;
    }

    public function getSignatureAlgorithm(): SignatureAlgorithm {
        return $this->signatureAlgorithm;
    }

    public function setSignatureAlgorithm($signatureAlgorithm): void {
        $this->signatureAlgorithm = $signatureAlgorithm;
    }

    public function getIssuer(): Issuer {
        return $this->issuer;
    }

    public function setIssuer($issuer): void {
        $this->issuer = $issuer;
    }

    public function getValidity(): Validity {
        return $this->validity;
    }

    public function setValidity($validity): void {
        $this->validity = $validity;
    }

    public function getSubject(): Subject {
        return $this->subject;
    }

    public function setSubject($subject): void {
        $this->subject = $subject;
    }

    public function getSubjectPublicKeyInfo(): SubjectPublicKeyInfo {
        return $this->subjectPublicKeyInfo;
    }

    public function setSubjectPublicKeyInfo($subjectPublicKeyInfo): void {
        $this->subjectPublicKeyInfo = $subjectPublicKeyInfo;
    }
}