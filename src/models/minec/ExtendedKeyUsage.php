<?php

namespace Datolab\DtesvSigner\models\minec;

class ExtendedKeyUsage
{
    private string $clientAuth;

    private string $emailProtection;

    public function __construct(string $clientAuth = '', $emailProtection = '' )
    {
        $this->clientAuth = $clientAuth;
        $this->emailProtection = $emailProtection;
    }

    public function getClientAuth(): string
    {
        return $this->clientAuth;
    }

    public function setClientAuth(string $clientAuth): void
    {
        $this->clientAuth = $clientAuth;
    }

    public function getEmailProtection()
    {
        return $this->emailProtection;
    }

    public function setEmailProtection(string $emailProtection): void
    {
        $this->emailProtection = $emailProtection;
    }
}