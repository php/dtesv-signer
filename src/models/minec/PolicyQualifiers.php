<?php

namespace Datolab\DtesvSigner\models\minec;

class PolicyQualifiers
{
    private string $cpsUri;

    private string $userNotice;

    public function __construct(
        string $cpsUri = "www2.mh.gob.sv/dpc",
        string $userNotice = "CERTIFICADO PARA FACTURACIÓN ELECTRÓNICA")
    {
        $this->cpsUri = $cpsUri;
        $this->userNotice = $userNotice;
    }

    /**
     * @return string
     */
    public function getCpsUri(): string
    {
        return $this->cpsUri;
    }

    /**
     * @param string $cpsUri
     */
    public function setCpsUri(string $cpsUri): void
    {
        $this->cpsUri = $cpsUri;
    }

    /**
     * @return string
     */
    public function getUserNotice(): string
    {
        return $this->userNotice;
    }

    /**
     * @param string $userNotice
     */
    public function setUserNotice(string $userNotice): void
    {
        $this->userNotice = $userNotice;
    }
}