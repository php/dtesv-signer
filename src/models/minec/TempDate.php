<?php

namespace Datolab\DtesvSigner\models\minec;

class TempDate
{
    private string $nano;

    private string $epochSecond;

    public function __construct($nano = '', $epochSecond = '')
    {
        $this->nano = $nano;
        $this->epochSecond = $epochSecond;
    }

    /**
     * @return string
     */
    public function getNano(): string
    {
        return $this->nano;
    }

    /**
     * @param string $nano
     */
    public function setNano(string $nano): void
    {
        $this->nano = $nano;
    }

    /**
     * @return string
     */
    public function getEpochSecond(): string
    {
        return $this->epochSecond;
    }

    /**
     * @param string $epochSecond
     */
    public function setEpochSecond(string $epochSecond): void
    {
        $this->epochSecond = $epochSecond;
    }
}