<?php

namespace Datolab\DtesvSigner\models\minec;

class PolicyInformation
{
    private string $policyIdentifier;

    private PolicyQualifiers $policyQualifiers;

    public function __construct(string $policyIdentifier = '', PolicyQualifiers $policyQualifiers = null)
    {
        $this->policyIdentifier = $policyIdentifier;
        $this->policyQualifiers = empty($policyQualifiers) ? new PolicyQualifiers() : $policyQualifiers ;
    }

    public function getPolicyIdentifier(): string
    {
        return $this->policyIdentifier;
    }

    public function setPolicyIdentifier(string $policyIdentifier): void
    {
        $this->policyIdentifier = $policyIdentifier;
    }

    public function getPolicyQualifiers(): PolicyQualifiers
    {
        return $this->policyQualifiers;
    }

    /**
     * @param PolicyQualifiers $policyQualifiers
     */
    public function setPolicyQualifiers(PolicyQualifiers $policyQualifiers): void
    {
        $this->policyQualifiers = $policyQualifiers;
    }
}