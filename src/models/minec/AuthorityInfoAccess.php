<?php

namespace Datolab\DtesvSigner\models\minec;

class AuthorityInfoAccess
{
    private $accessDescription = [];

    public function __construct(AccessDescription ...$accessDescription)
    {
        $this->accessDescription = $accessDescription;
    }

    public function addAccessDescription(AccessDescription ...$accessDescription): void
    {
        $this->accessDescription[] = $accessDescription;
    }

    public function getAccessDescription(): array
    {
        return $this->accessDescription;
    }

    public function setAccessDescription(AccessDescription ...$accessDescription): void
    {
        $this->accessDescription = $accessDescription;
    }
}