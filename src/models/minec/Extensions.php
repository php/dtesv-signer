<?php

namespace Datolab\DtesvSigner\models\minec;

class Extensions
{
    private AuthorityKeyIdentifier $authorityKeyIdentifier;
    private SubjectKeyIdentifier $subjectKeyIdentifier;
    private KeyUsage $keyUsage;
    private CertificatePolicies $certificatePolicies;
    private SubjectAlternativeNames $subjectAlternativeNames;
    private ExtendedKeyUsage $extendedKeyUsage;
    private CtrlDistributionPoint $ctrlDistributionPoint;
    private AuthorityInfoAccess $authorityInfoAccess;
    private QualifiedCertificateStatements $qualifiedCertificateStatements;
    private BasicConstraints $basicConstraints;

    public function __construct1($rfc822Name = null, $ctrlDistributionPoint = null, $accessLocation = null)
    {
        $this->authorityKeyIdentifier = new AuthorityKeyIdentifier();
        $this->subjectKeyIdentifier = new SubjectKeyIdentifier();
        $this->keyUsage = new KeyUsage();
        $this->certificatePolicies = new CertificatePolicies();
        $this->subjectAlternativeNames = new SubjectAlternativeNames($rfc822Name);
        $this->extendedKeyUsage = new ExtendedKeyUsage();
        $this->ctrlDistributionPoint = new CtrlDistributionPoint($ctrlDistributionPoint);

        $this->authorityInfoAccess = new AuthorityInfoAccess();
        $this->authorityInfoAccess->addAccessDescription(new AccessDescription($accessLocation));

        $this->qualifiedCertificateStatements = new QualifiedCertificateStatements(new QcPDS());
        $this->basicConstraints = new BasicConstraints();
    }

    public function __construct0() {
        // PHP does not support multiple constructors, so we use a different method name
    }

    public function __construct($authorityKeyIdentifier,
                                 $subjectKeyIdentifier,
                                 $keyUsage,
                                 $certificatePolicies,
                                 $subjectAlternativeNames,
                                 $extendedKeyUsage,
                                 $ctrlDistributionPoint,
                                 $authorityInfoAccess,
                                 $qualifiedCertificateStatements,
                                 $basicConstraints)
    {
        $this->authorityKeyIdentifier = $authorityKeyIdentifier;
        $this->subjectKeyIdentifier = $subjectKeyIdentifier;
        $this->keyUsage = $keyUsage;
        $this->certificatePolicies = $certificatePolicies;
        $this->subjectAlternativeNames = $subjectAlternativeNames;
        $this->extendedKeyUsage = $extendedKeyUsage;
        $this->ctrlDistributionPoint = $ctrlDistributionPoint;
        $this->authorityInfoAccess = $authorityInfoAccess;
        $this->qualifiedCertificateStatements = $qualifiedCertificateStatements;
        $this->basicConstraints = $basicConstraints;
    }

    public function getAuthorityKeyIdentifier(): AuthorityKeyIdentifier
    {
        return $this->authorityKeyIdentifier;
    }

    public function setAuthorityKeyIdentifier($authorityKeyIdentifier): void
    {
        $this->authorityKeyIdentifier = $authorityKeyIdentifier;
    }

    public function getSubjectKeyIdentifier(): SubjectKeyIdentifier
    {
        return $this->subjectKeyIdentifier;
    }

    public function setSubjectKeyIdentifier($subjectKeyIdentifier): void
    {
        $this->subjectKeyIdentifier = $subjectKeyIdentifier;
    }

    public function getKeyUsage(): KeyUsage
    {
        return $this->keyUsage;
    }

    public function setKeyUsage($keyUsage): void
    {
        $this->keyUsage = $keyUsage;
    }

    public function getCertificatePolicies(): CertificatePolicies
    {
        return $this->certificatePolicies;
    }

    public function setCertificatePolicies($certificatePolicies): void
    {
        $this->certificatePolicies = $certificatePolicies;
    }

    public function getSubjectAlternativeNames(): SubjectAlternativeNames
    {
        return $this->subjectAlternativeNames;
    }

    public function setSubjectAlternativeNames($subjectAlternativeNames): void
    {
        $this->subjectAlternativeNames = $subjectAlternativeNames;
    }

    public function getExtendedKeyUsage(): ExtendedKeyUsage
    {
        return $this->extendedKeyUsage;
    }

    public function setExtendedKeyUsage($extendedKeyUsage): void
    {
        $this->extendedKeyUsage = $extendedKeyUsage;
    }

    public function getCrlDistributionPoint(): CtrlDistributionPoint
    {
        return $this->ctrlDistributionPoint;
    }

    public function setCtrlDistributionPoint($ctrlDistributionPoint): void
    {
        $this->ctrlDistributionPoint = $ctrlDistributionPoint;
    }

    public function getAuthorityInfoAccess(): AuthorityInfoAccess
    {
        return $this->authorityInfoAccess;
    }

    public function setAuthorityInfoAccess($authorityInfoAccess): void
    {
        $this->authorityInfoAccess = $authorityInfoAccess;
    }

    public function getQualifiedCertificateStatements(): QualifiedCertificateStatements
    {
        return $this->qualifiedCertificateStatements;
    }

    public function setQualifiedCertificateStatements($qualifiedCertificateStatements): void
    {
        $this->qualifiedCertificateStatements = $qualifiedCertificateStatements;
    }

    public function getBasicConstraints(): BasicConstraints
    {
        return $this->basicConstraints;
    }

    public function setBasicConstraints($basicConstraints): void
    {
        $this->basicConstraints = $basicConstraints;
    }
}
