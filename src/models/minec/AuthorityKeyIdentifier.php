<?php

namespace Datolab\DtesvSigner\models\minec;

class AuthorityKeyIdentifier
{
    private string $keyIdentifier;

    public function __construct($keyIdentifier) {
        $this->setKeyIdentifier($keyIdentifier);
    }

    public function getKeyIdentifier(): string
    {
        return $this->keyIdentifier;
    }

    public function setKeyIdentifier(string $keyIdentifier): void
    {
        $this->keyIdentifier = $keyIdentifier;
    }
}
