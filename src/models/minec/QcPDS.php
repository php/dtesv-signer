<?php

namespace Datolab\DtesvSigner\models\minec;

class QcPDS
{
    private string $pdsLocation;
    private string $url;
    private string $language;

    public function __construct($pdsLocation = null, $url = "https://example.com", $language = "ES") {
        $this->pdsLocation = $pdsLocation;
        $this->url = $url;
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getPdsLocation(): string
    {
        return $this->pdsLocation;
    }

    /**
     * @param string $pdsLocation
     */
    public function setPdsLocation(string $pdsLocation): void
    {
        $this->pdsLocation = $pdsLocation;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }
}