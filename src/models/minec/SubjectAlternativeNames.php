<?php

namespace Datolab\DtesvSigner\models\minec;

class SubjectAlternativeNames
{
    private string $rfc822Name;

    public function __construct(string $rfc822Name = '')
    {
        $this->rfc822Name = $rfc822Name;
    }

    /**
     * @return string
     */
    public function getRfc822Name(): string
    {
        return $this->rfc822Name;
    }

    /**
     * @param string $rfc822Name
     */
    public function setRfc822Name(string $rfc822Name): void
    {
        $this->rfc822Name = $rfc822Name;
    }
}