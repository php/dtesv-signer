<?php

namespace Datolab\DtesvSigner\models\minec;

class KeyUsage
{
    private  int $digitalSignature;
    private int $contentCommitment;
    private int $dataEncipherment;
    private int $keyAgreement;
    private int $keyCertificateSignature;
    private int $crlSignature;
    private int $encipherOnly;
    private int $decipherOnly;

    public function __construct(
        $digitalSignature = 1,
        $contentCommitment = 1,
        $dataEncipherment = 0,
        $keyAgreement = 0,
        $keyCertificateSignature = 0,
        $crlSignature = 0,
        $encipherOnly = 0,
        $decipherOnly = 0
    ) {
        $this->digitalSignature = (int)$digitalSignature;
        $this->contentCommitment = (int)$contentCommitment;
        $this->dataEncipherment = (int)$dataEncipherment;
        $this->keyAgreement = (int)$keyAgreement;
        $this->keyCertificateSignature = (int)$keyCertificateSignature;
        $this->crlSignature = (int)$crlSignature;
        $this->encipherOnly = (int)$encipherOnly;
        $this->decipherOnly = (int)$decipherOnly;
    }

    public function getDigitalSignature(): int
    {
        return $this->digitalSignature;
    }

    public function setDigitalSignature($digitalSignature): void
    {
        $this->digitalSignature = $digitalSignature;
    }

    public function getContentCommitment(): int
    {
        return $this->contentCommitment;
    }

    public function setContentCommitment($contentCommitment): void
    {
        $this->contentCommitment = $contentCommitment;
    }

    public function getDataEncipherment(): int
    {
        return $this->dataEncipherment;
    }

    public function setDataEncipherment($dataEncipherment): void
    {
        $this->dataEncipherment = $dataEncipherment;
    }

    public function getKeyAgreement(): int
    {
        return $this->keyAgreement;
    }

    public function setKeyAgreement($keyAgreement): void
    {
        $this->keyAgreement = $keyAgreement;
    }

    public function getKeyCertificateSignature(): int
    {
        return $this->keyCertificateSignature;
    }

    public function setKeyCertificateSignature($keyCertificateSignature): void
    {
        $this->keyCertificateSignature = $keyCertificateSignature;
    }

    public function getCrlSignature(): int
    {
        return $this->crlSignature;
    }

    public function setCrlSignature($crlSignature): void
    {
        $this->crlSignature = $crlSignature;
    }

    public function getEncipherOnly(): int
    {
        return $this->encipherOnly;
    }

    public function setEncipherOnly($encipherOnly): void
    {
        $this->encipherOnly = $encipherOnly;
    }

    public function getDecipherOnly(): int
    {
        return $this->decipherOnly;
    }

    public function setDecipherOnly($decipherOnly): void
    {
        $this->decipherOnly = $decipherOnly;
    }
}
