<?php

namespace Datolab\DtesvSigner\models\minec;

class SignatureAlgorithm
{
    private string $algorithm;

    private string $parameters;

    public function __construct(string $algorithm = '', string $parameters = '')
    {
        $this->algorithm = $algorithm;
        $this->parameters = $parameters;
    }

    /**
     * @return string
     */
    public function getAlgorithm(): string
    {
        return $this->algorithm;
    }

    /**
     * @param string $algorithm
     */
    public function setAlgorithm(string $algorithm): void
    {
        $this->algorithm = $algorithm;
    }

    /**
     * @return string
     */
    public function getParameters(): string
    {
        return $this->parameters;
    }

    /**
     * @param string $parameters
     */
    public function setParameters(string $parameters): void
    {
        $this->parameters = $parameters;
    }
}