<?php

namespace Datolab\DtesvSigner\models\minec;

class Validity
{
    private TempDate $notBefore;

    private TempDate $notAfter;

    public function __construct(TempDate $notBefore = null, TempDate $notAfter = null)
    {
        $this->notBefore = empty($notBefore) ? new TempDate() : $notBefore;
        $this->notAfter = empty($notAfter) ? new TempDate() : $notAfter;
    }

    /**
     * @return TempDate
     */
    public function getNotBefore(): TempDate
    {
        return $this->notBefore;
    }

    /**
     * @param TempDate $notBefore
     */
    public function setNotBefore(TempDate $notBefore): void
    {
        $this->notBefore = $notBefore;
    }

    /**
     * @return TempDate
     */
    public function getNotAfter(): TempDate
    {
        return $this->notAfter;
    }

    /**
     * @param TempDate $notAfter
     */
    public function setNotAfter(TempDate $notAfter): void
    {
        $this->notAfter = $notAfter;
    }
}