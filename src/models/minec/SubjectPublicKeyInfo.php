<?php

namespace Datolab\DtesvSigner\models\minec;

class SubjectPublicKeyInfo
{
    private AlgorithmIdentifier $algorithmIdentifier;
    private array $subjectPublicKey;

    public function __construct($algorithmIdentifier = null, $subjectPublicKey = null) {
        $this->algorithmIdentifier = $algorithmIdentifier ?? new AlgorithmIdentifier();
        $this->subjectPublicKey = $subjectPublicKey;
    }

    /**
     * @return AlgorithmIdentifier
     */
    public function getAlgorithmIdentifier(): AlgorithmIdentifier
    {
        return $this->algorithmIdentifier;
    }

    /**
     * @param AlgorithmIdentifier $algorithmIdentifier
     */
    public function setAlgorithmIdentifier(AlgorithmIdentifier $algorithmIdentifier): void
    {
        $this->algorithmIdentifier = $algorithmIdentifier;
    }

    /**
     * @return array
     */
    public function getSubjectPublicKey(): array
    {
        return $this->subjectPublicKey;
    }

    /**
     * @param array $subjectPublicKey
     */
    public function setSubjectPublicKey(array $subjectPublicKey): void
    {
        $this->subjectPublicKey = $subjectPublicKey;
    }
}