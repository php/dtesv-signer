<?php

namespace Datolab\DtesvSigner\models\minec;

class QualifiedCertificateStatements
{
    private string $qcCompliance;
    private string $qcEuRetentionPeriod;
    private QcPDS $qcPDS;
    private string $qcType;

    public function __construct($qcPDS = null, $qcCompliance = "", $qcEuRetentionPeriod = "10", $qcType = "id-etsi-qct-esign") {
        $this->qcCompliance = (string)$qcCompliance;
        $this->qcEuRetentionPeriod = (string)$qcEuRetentionPeriod;
        $this->qcPDS = $qcPDS;
        $this->qcType = (string)$qcType;
    }

    /**
     * @return string
     */
    public function getQcCompliance(): string
    {
        return $this->qcCompliance;
    }

    /**
     * @param string $qcCompliance
     */
    public function setQcCompliance(string $qcCompliance): void
    {
        $this->qcCompliance = $qcCompliance;
    }

    /**
     * @return string
     */
    public function getQcEuRetentionPeriod(): string
    {
        return $this->qcEuRetentionPeriod;
    }

    /**
     * @param string $qcEuRetentionPeriod
     */
    public function setQcEuRetentionPeriod(string $qcEuRetentionPeriod): void
    {
        $this->qcEuRetentionPeriod = $qcEuRetentionPeriod;
    }

    /**
     * @return QcPDS
     */
    public function getQcPDS(): QcPDS
    {
        return $this->qcPDS;
    }

    /**
     * @param QcPDS $qcPDS
     */
    public function setQcPDS(QcPDS $qcPDS): void
    {
        $this->qcPDS = $qcPDS;
    }

    /**
     * @return string
     */
    public function getQcType(): string
    {
        return $this->qcType;
    }

    /**
     * @param string $qcType
     */
    public function setQcType(string $qcType): void
    {
        $this->qcType = $qcType;
    }
}
