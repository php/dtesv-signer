<?php

namespace Datolab\DtesvSigner\models\minec;

class CtrlDistributionPoint
{
    private $distributionPoint = [];

    public function __construct(array ...$distributionPoint)
    {
        $this->distributionPoint = $distributionPoint;
    }

    public function add(string $distributionPoint): void
    {
        $this->distributionPoint[] = $distributionPoint;
    }

    public function getDistributionPoint(): array
    {
        return $this->distributionPoint;
    }

    public function setDistributionPoint(string $distributionPoint): void
    {
        $this->distributionPoint = $distributionPoint;
    }
}
