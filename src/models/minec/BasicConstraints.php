<?php

namespace Datolab\DtesvSigner\models\minec;

class BasicConstraints
{
    private bool $ca;

    public function __construct(bool $ca = false) {
        //parent::__construct();
        $this->ca = $ca;
    }

    public function getCa(): bool
    {
        return $this->ca;
    }

    public function setCa(bool $ca): void
    {
        $this->ca = $ca;
    }
}