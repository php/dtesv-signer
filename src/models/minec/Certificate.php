<?php

namespace Datolab\DtesvSigner\models\minec;

require_once __DIR__.'/../../business/Certificate.php';

class Certificate extends \Datolab\DtesvSigner\business\Certificate
{
    private BasicStructure $basicStructure;

    private Extensions $extensions;

	public function __construct(BasicStructure $basicStructure, Extensions $extensions)
	{
		$this->setBasicStructure($basicStructure);
		$this->setExtensions($extensions);
	}

	public function getBasicStructure(): BasicStructure
    {
        return $this->basicStructure;
    }

    public function setBasicStructure(BasicStructure $basicStructure): void
    {
        $this->basicStructure = $basicStructure;
    }

    public function getExtensions(): Extensions
    {
        return $this->extensions;
    }

    public function setExtensions(Extensions $extensions): void
    {
        $this->extensions = $extensions;
    }
}
