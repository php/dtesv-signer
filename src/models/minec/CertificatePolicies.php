<?php

namespace Datolab\DtesvSigner\models\minec;

class CertificatePolicies
{
    private $policyInfos = [];

    public function __construct(?PolicyInformation ...$policyInfos)
    {
        $this->policyInfos = $policyInfos;
    }

    public function add(PolicyInformation ...$policyInfos): void
    {
        $this->policyInfos[] = $policyInfos;
    }

    public function getPolicyInfos(): array
    {
        return $this->policyInfos;
    }

    public function setPolicyInfos(PolicyInformation ...$policyInfos): void
    {
        $this->policyInfos = $policyInfos;
    }
}
