<?php

namespace Datolab\DtesvSigner\constants;

class Errors
{
    const COD_801_CERT_ERROR_NO_ENCOTRADO = "801";
    const COD_802_NO_VALIDO = "802";
    const COD_803_ERROR_LLAVE_PRUBLICA = "803";
    const COD_804_ERROR_NO_CATALOGADO = "804";
    const COD_805_ERROR_CERTIFCADO_DUPLICADO = "805";
    const COD_806_ERROR_GENERACION_CERTIFICADO = "806";
    const COD_807_ERROR_DESCARGAR_ARCHIVO = "807";
    const COD_808_ERROR_SUBUR_ARCHIVO = "808";
    const COD_809_DATOS_REQUERIDOS = "809";
    const COD_810_CONVERTIR_JSON_A_STRING = "810";
    const COD_811_CONVERTIR_STRING_A_JSON = "811";
    const COD_812_NO_FILE = "812";

    private string $text;
    private string $code;

    private function __construct($code, $text) {
        $this->code = $code;
        $this->text = $text;
    }

    public function getText(): string {
        return $this->text;
    }

    public function getCode(): string {
        return $this->code;
    }

    public static function COD_801_CERT_ERROR_NO_ENCOTRADO(): Errors {
        return new self("801", "No existe certificado activo");
    }

    public static function COD_802_NO_VALIDO(): Errors {
        return new self("802", "No valido");
    }

    public static function COD_803_ERROR_LLAVE_PRUBLICA(): Errors {
        return new self("803", "No existe llave publica para este nit");
    }

    public static function COD_804_ERROR_NO_CATALOGADO(): Errors {
        return new self("804", "Error no catalogado");
    }

    public static function COD_805_ERROR_CERTIFCADO_DUPLICADO(): Errors {
        return new self("805", "Ya existe una certificado activo");
    }

    public static function COD_806_ERROR_GENERACION_CERTIFICADO(): Errors {
        return new self("806", "Generación de certificados satisfactoria");
    }

    public static function COD_807_ERROR_DESCARGAR_ARCHIVO(): Errors {
        return new self("807", "Error en la descarga de archivo");
    }

    public static function COD_808_ERROR_SUBUR_ARCHIVO(): Errors {
        return new self("808", "Error en al subir el archivo");
    }

    public static function COD_809_DATOS_REQUERIDOS(): Errors {
        return new self("809", "Son datos requeridos");
    }

    public static function COD_810_CONVERTIR_JSON_A_STRING(): Errors {
        return new self("810", "Problemas al convertir Json a String");
    }

    public static function COD_811_CONVERTIR_STRING_A_JSON(): Errors {
        return new self("811", "Problemas al convertir String a Json");
    }

    public static function COD_812_NO_FILE(): Errors {
        return new self("812", "No se encontro el archivo");
    }
}