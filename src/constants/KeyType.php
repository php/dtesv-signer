<?php

namespace Datolab\DtesvSigner\constants;

class KeyType
{
	protected string $type;

	public function __construct($type)
	{
		$this->setType($type);
	}

	/**
	 * @param string $type
	 */
	public function setType(string $type): void
	{
		$this->type = $type;
	}

	/**
	 * @return string
	 */
	public function getType(): string
	{
		return $this->type;
	}
    public function __toString()
    {
        return $this->type;
    }
}
