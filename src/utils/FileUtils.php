<?php

namespace Datolab\DtesvSigner\utils;

use Exception;

class FileUtils
{
    const MEDIA_TYPE_APPLICATION_KEY = "application/pkcs8";
    const MEDIA_TYPE_APPLICATION_CRT = "application/x-x509-ca-cert";

//    public function __construct() {
//        parent::__construct();
//    }

    /**
     * @throws Exception
     */
    public function createFile($path, $content): void {
        $file = fopen($path, 'w');
        if ($file === false) {
            throw new Exception('File not found: ' . $path);
        }
        fwrite($file, $content);
        fflush($file);
        fclose($file);
    }

    /**
     * @throws Exception
     */
    public function readFile($path): string
    {
        $content = '';
        $file = fopen($path, 'r');
        if ($file === false) {
            throw new Exception('File not found: ' . $path);
        }
        while (($line = fgets($file)) !== false) {
            $content .= $line;
        }
        fclose($file);
        return $content;
    }
}