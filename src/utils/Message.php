<?php

namespace Datolab\DtesvSigner\utils;

class Message
{
    public function ok($body): ResponseBody {
        $responseBody = new ResponseBody();
        $responseBody->setStatus(ResponseBody::STATUS_OK);
        $responseBody->setBody($body);
        return $responseBody;
    }

    public function okFromJson($body): ResponseBody {
        $responseBody = new ResponseBody();
        $responseBody->setStatus(ResponseBody::STATUS_OK);
        $responseBody->setBody(json_decode($body, false));
        return $responseBody;
    }

    public function error($code, $message = null): ResponseBody {
        $responseBody = new ResponseBody();
        if ($message === null) {
            // If only one parameter is passed, it's treated as the body
            $body = $code;
        } else {
            // If both parameters are passed, create a BodyMessage object
            $body = new BodyMessage($code, $message);
        }
        $responseBody->setStatus(ResponseBody::STATUS_ERROR);
        $responseBody->setBody($body);
        return $responseBody;
    }

    public function errorFromJson($body): ResponseBody {
        $responseBody = new ResponseBody();
        $responseBody->setStatus(ResponseBody::STATUS_ERROR);
        $responseBody->setBody(json_decode($body, false));
        return $responseBody;
    }

    public function errorFromErrors($error): ResponseBody {
        $responseBody = new ResponseBody();
        $body = new BodyMessage($error->getCode(), $error->getText());
        $responseBody->setStatus(ResponseBody::STATUS_ERROR);
        $responseBody->setBody($body);
        return $responseBody;
    }
}