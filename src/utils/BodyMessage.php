<?php

namespace Datolab\DtesvSigner\utils;

class BodyMessage implements \Serializable
{
    private string $code;
    private object $message;

    public function __construct($code, $message) {
        $this->code = $code;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return object
     */
    public function getMessage(): object
    {
        return $this->message;
    }

    /**
     * @param object $message
     */
    public function setMessage(object $message): void
    {
        $this->message = $message;
    }

    /**
     * @inheritDoc
     */
    public function serialize()
    {
        // TODO: Implement serialize() method.
    }

    /**
     * @inheritDoc
     */
    public function unserialize($data)
    {
        // TODO: Implement unserialize() method.
    }
}