<?php

namespace Datolab\DtesvSigner\utils;

class ResponseBody
{
    const STATUS_OK = "OK";
    const STATUS_ERROR = "ERROR";
    private string $status;
    private object $body;

    public function __construct() {
        // PHP's constructor is automatically called, no need for super();
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return object
     */
    public function getBody(): object
    {
        return $this->body;
    }

    /**
     * @param object $body
     */
    public function setBody(object $body): void
    {
        $this->body = $body;
    }
}