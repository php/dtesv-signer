<?php

namespace Datolab\DtesvSigner\controller;

use Datolab\DtesvSigner\Business\Certificate;
use \Datolab\DtesvSigner\Business\SignDocument as SignDocBusiness;
use Datolab\DtesvSigner\Constants\Errors;
use \Datolab\DtesvSigner\Validations\SignDocument as SignDocValidations;
use Datolab\DtesvSigner\Filter\SignDocument as SignDocFilter;
use http\Client\Response;
use lib\Logger;

/**
 * @Route("/signdocument", methods={"POST"})
 */
class SignDocument extends Base
{
    private Logger $logger;
    private Certificate $businessCertificate;
    private SignDocBusiness $business;
    private SignDocValidations $validation;

    public function __construct(
        Logger $logger,
        Certificate $businessCertificate,
        SignDocBusiness $business,
        SignDocValidations $validation
    ) {
        $this->logger = $logger;
        $this->businessCertificate = $businessCertificate;
        $this->business = $business;
        $this->validation = $validation;
    }

    /**
     * @Route("/", methods={"POST"})
     */
    public function sign(SignDocFilter $filter): Response
    {
        //$filter = json_decode($request->getContent(), true);
        //$violations = $this->validation->v1validate($filter, new Assert\Collection([
            // Define your constraints here
        //]));

        //if (count($violations) > 0) {
            // Handle validation errors
        //    return new Response('Validation errors', Response::HTTP_BAD_REQUEST);
        //}

        try {
            $this->validation->v5validate($filter);
            if ($this->validation->isValid()) {
                $certificate = $this->businessCertificate->getCertificate($filter);
                if ($certificate != null) {
                    $dteString = json_encode($filter['dteJson'], JSON_PRETTY_PRINT);
                    $dteObject = json_decode($dteString);
                    if ($dteObject != null) {
                        $this->logger->info("dteObject != null");
                        $firma = $this->business->signJSONString($certificate, $dteString);
                        return new Response(json_encode(['ok' => $firma]), Response::HTTP_OK);
                    }
                } else {
                    return new Response(json_encode(['error' => Errors::COD_803_ERROR_LLAVE_PRUBLICA]), Response::HTTP_OK);
                }
            } else {
                return new Response(json_encode(['error' => Errors::COD_809_DATOS_REQUERIDOS, 'required' => $this->validation->getRequired()]), Response::HTTP_OK);
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return new Response(json_encode(['error' => Errors::COD_804_ERROR_NO_CATALOGADO, 'message' => $e->getMessage()]), Response::HTTP_OK);
        }

        return new Response(json_encode(['error' => Errors::COD_804_ERROR_NO_CATALOGADO]), Response::HTTP_OK);
    }
}
