<?php

namespace Datolab\DtesvSigner\security;

use Exception;
use OpenSSLAsymmetricKey;

class KeyGenerator
{
    const RSA = "RSA";
    const SHA1WITHRSA = "SHA1withRSA";
    const SHA256WITHRSA = "SHA256withRSA";
    const KEYSIZE = 2048;

    public function byteToPrivateKey($bytes, $algorithm = self::RSA): false|OpenSSLAsymmetricKey {
//        $privateKey = null;

//		$privateKeyString = <<<EOD
//-----BEGIN PRIVATE KEY-----
//${bytes[0]}
//-----END PRIVATE KEY-----
//EOD;

// Optional passphrase for encrypted keys
//		$passphrase = $bytes[1];

		$privateKey = openssl_pkey_get_private($bytes[0], $bytes[1]);

		if ($privateKey === false) {
			error_log("Failed to load private key");
			// Check for OpenSSL errors
			while ($error = openssl_error_string()) {
				error_log("OpenSSL Error: " . $error);
			}
		} else {
			error_log("Private key loaded successfully");
		}


        try {
//			$bytes[0] = $privateKeyString;
            $privateKey = openssl_pkey_get_private($bytes, $algorithm);
//			pr(['PRIVATE KEY',$privateKey]);
        } catch (Exception $e) {
//			pr(['E',$e]);
            error_log($e->getMessage());
        }

        return $privateKey;
    }

    public function byteToPublicKey($bytes, $algorithm = self::RSA): false|OpenSSLAsymmetricKey {
        $publicKey = null;
        try {
            $publicKey = openssl_pkey_get_public($bytes);
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
        return $publicKey;
    }
}
