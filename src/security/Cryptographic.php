<?php

namespace Datolab\DtesvSigner\security;

class Cryptographic
{
    const SHA256 = "sha256";
    const SHA512 = "sha512";

    public function encrypt($p, $sha = self::SHA256): string
    {
        $encodedHash = hash($sha, $p, true);
        return $this->bytesToHex($encodedHash);
    }

    private function bytesToHex($hash): string
    {
        $hexString = '';
        for ($i = 0; $i < strlen($hash); $i++) {
            $hex = dechex(ord($hash[$i]));
            if(strlen($hex) == 1) $hexString .= '0';
            $hexString .= $hex;
        }
        return $hexString;
    }
}