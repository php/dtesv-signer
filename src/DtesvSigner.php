<?php
namespace Datolab\DtesvSigner;

use Datolab\DtesvSigner\business\Certificate;
use Datolab\DtesvSigner\business\SignDocument as SignDocBusiness;
use Datolab\DtesvSigner\constants\Errors;
use Datolab\DtesvSigner\filter\SignDocument as SignDocFilter;
use Datolab\DtesvSigner\models\MHCertificate;
use Datolab\DtesvSigner\validations\SignDocument as SignDocValidations;
use lib\Logger;

class DtesvSigner
{
    private Logger $logger;
    private Certificate $businessCertificate;
    private SignDocBusiness $business;
    private SignDocValidations $validation;

    public function __construct(
        Logger             $logger,
        Certificate        $businessCertificate,
        SignDocBusiness    $business,
        SignDocValidations $validation
    )
    {
        $this->logger = $logger;
        $this->businessCertificate = $businessCertificate;
        $this->business = $business;
        $this->validation = $validation;
    }

    /**
     * @Route("/", methods={"POST"})
     */
    public function sign(SignDocFilter $filter): string
    {
		global $conf;
        //$filter = json_decode($request->getContent(), true);
        //$violations = $this->validation->v1validate($filter, new Assert\Collection([
        // Define your constraints here
        //]));

        //if (count($violations) > 0) {
        // Handle validation errors
        //    return new Response('Validation errors', Response::HTTP_BAD_REQUEST);
        //}

        try {
            $this->validation->v5validate($filter);
            if ($this->validation->isValid()) {
                $certificate = $this->businessCertificate->getCertificate($filter);
                if ($certificate != null) {
                    $dteString = json_encode($filter->getDteJson(), JSON_PRETTY_PRINT);
                    $dteObject = json_decode($dteString);
                    if ($dteObject != null) {
                        $this->logger->info("dteObject != null");
                        $firma = $this->business->signJSONString($certificate, $dteString);
//						var_dump($firma);
                        return json_encode(['ok' => $firma]);
                    }
                } else {
                    return json_encode(['error' => Errors::COD_803_ERROR_LLAVE_PRUBLICA]);
                }
            } else {
                return json_encode(['error' => Errors::COD_809_DATOS_REQUERIDOS, 'required' => $this->validation->getRequired()]);
            }
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return json_encode(['error' => Errors::COD_804_ERROR_NO_CATALOGADO, 'message' => $e->getMessage()]);
        }

        return json_encode(['error' => Errors::COD_804_ERROR_NO_CATALOGADO]);
    }
}
