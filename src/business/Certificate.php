<?php

namespace Datolab\DtesvSigner\business;

use Datolab\DtesvSigner\Constants\Definitions;
use Datolab\DtesvSigner\Models\MHCertificate;
use Datolab\DtesvSigner\security\Cryptographic;
use Datolab\DtesvSigner\Utils\FileUtils;
use Datolab\DtesvSigner\filter\SignDocument as SignDocFilter;
use Exception;
use lib\Logger;
//use Monolog\Handler\StreamHandler;

require_once __DIR__.'/../constants/Definitions.php';
require_once __DIR__.'/../constants/Errors.php';

class Certificate
{
    private Cryptographic $cryptographic;
    private FileUtils $fileUtils;
    private static Logger $logger;

    public function __construct($cryptographic, $fileUtils) {
        $this->cryptographic = $cryptographic;
        $this->fileUtils = $fileUtils;
        self::$logger = new Logger('BusinessCertificate');
//        self::$logger->pushHandler(new StreamHandler('path/to/your.log', Logger::INFO));
    }

    /**
     * @throws \Exception
	 * @returns null|MHCertificate
     */
    public function getCertificate(SignDocFilter $filter): ?MHCertificate {
        $crypto = $this->cryptographic->encrypt($filter->getPrivatePassword(), 'sha512');
        $path = Definitions::DIRECTORY_UPLOADS . DIRECTORY_SEPARATOR . $filter->getNit() . ".crt";
		try {
			$certificate = createMHCertificateFromXML($path);
		} catch (Exception $e) {
			echo 'Error: ' . $e->getMessage();
		}
        if ($certificate->getPrivateKey()->getClave() === $crypto) {
            return $certificate;
        }
        self::$logger->info("Password no valido: " . $certificate->getNit());
        return null;
    }
}
