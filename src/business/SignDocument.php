<?php

namespace Datolab\DtesvSigner\business;

use Datolab\DtesvSigner\security\KeyGenerator;
use Datolab\DtesvSigner\Utils\FileUtils;
use Exception;
use Firebase\JWT\JWT;
use lib\Logger;
use Seld\JsonLint\JsonParser;
use Seld\JsonLint\ParsingException;

require_once __DIR__.'/../../../../lib/dtesv.lib.php';
require_once __DIR__.'/../../../JWT/JWT.php';
requireDir(__DIR__.'/../../../jsonlint/src/Seld/JsonLint');

class SignDocument
{
    private FileUtils $fileUtils;
    private KeyGenerator $keyGenerator;
    private Logger $logger;

    public function __construct($fileUtils, $keyGenerator, Logger $logger) {
        $this->fileUtils = $fileUtils;
        $this->keyGenerator = $keyGenerator;
        $this->logger = $logger;
    }

    /**
     * @throws Exception
     */
    public function signJSON($certificate, $path): void {
        try {
            $content = $this->fileUtils->readFile($path);
            $key = $this->keyGenerator->byteToPrivateKey([$certificate->getPrivateKey()->getEncoded(), $certificate->getPrivateKey()->getClave()]);
            $jwt = JWT::encode($content, $key, 'RS512');
            $this->fileUtils->createFile($path, $jwt);
        } catch (Exception $e) {
            $this->logger->error("Error signing JSON: " . $e->getMessage());
            throw $e;
        }
    }

    /**
     * @throws Exception
     */
    public function signJSONString($certificate, $content): string {
        try {
//			pr($certificate);
            $key = $this->keyGenerator->byteToPrivateKey(
				[
					$certificate->getPrivateKey()->getFormatted(),
					$certificate->getPrivateKey()->getClave()
				]
			);

			// Remove the first and last double quotes if they exist
			if ($content[0] === '"' && $content[-1] === '"') {
				$jsonString = substr($content, 1, -1);
			}

			$jsonString = stripslashes($jsonString);
			$array = json_decode($jsonString, JSON_UNESCAPED_SLASHES);
			if (json_last_error() !== JSON_ERROR_NONE) {
				// Error handling
				echo "Error decoding JSON: [". json_last_error() . "] - " . json_last_error_msg() . "\n";
			}

			// Using jsonlint for more detailed error reporting
			$parser = new JsonParser();

			try {
				// Parse the JSON string
				$result = $parser->parse($jsonString, JsonParser::DETECT_KEY_CONFLICTS);
				// If no exception is thrown, the JSON is valid
//				echo "JSON is valid.\n";
//				print_r(json_decode($jsonString, true));
			} catch (ParsingException $e) {
				// Catch parsing exceptions and display detailed error information
				echo "JSON parsing error: " . $e->getMessage() . "\n";
				echo "Error details: " . print_r($e->getDetails(), true) . "\n";
			}
//			pr($key);
            return JWT::encode($array, $key, 'RS512');
        } catch (Exception $e) {
            $this->logger->error("Error signing JSON: " . $e->getMessage());
            throw $e;
        }
    }
}
