<?php

namespace Datolab\DtesvSigner\validations;

use lib\Logger;
abstract class Base
{
    const REQ_NIT = "NIT es requerido";
    const REQ_DATA = "Objeto se recibió vacío";
    const REQ_NIT_FORMAT = "Formato de NIT no valido - (00000000000000)  ";
    const REQ_JWS = "JSON WEB Signing es requerido";
    const REQ_DOCUMENT_NAME = "El nombre del docuemnto es requerido";
    const REQ_SIGN_NAME = "El nombre del firma es requerido";
    const REQ_JSON_DTE = "JsonDTE es requerido";
    const REQ_PRIVATE_KEY = "Clave privada es requerida";
    const REQ_PRIVATE_CONFIRMATION = "Clave privada y confirmación no son iguales";
    const REQ_PUBLIC_KEY = "Clave publica es requerida";
    const REQ_PUBLIC_CONFIRMATION = "Clave publica y confirmación no son iguales";
    const REQ_COMPACT_SERIALIZATION = "La Serialización Compacta es requerida";
    const REQ_SUBJECT_COUNTRY_NAME = "Nombre del país es requerido";
    const REQ_SUBJECT_ORG_NAME = "Nombre de la organización es requerido";
    const REQ_SUBJECT_ORG_UNIT = "Nombre de la unidad es requerido";
    const REQ_SUBJECT_ORG_IDENTIFIER = "Organization Identifier es requerido";
    const REQ_SUBJECT_SURNAME = "Apellido del firmante es un campo requerido";
    const REQ_SUBJECT_GIVEN = "Nombre del firmante es un campo requerido";
    const REQ_SUBJECT_COMMON_NAME = "CommonName es un campo requerido";
    const REQ_SUBJECT_DESCRIPTION = "NRC es un campo requerido";
    const REQ_SUBJECT_EMAIL_ORG = "Correo de organización es un campo requerido";

    protected array $required;
    protected string $validNIT;
    protected bool $valid;

    public function validateNIT($nit): array {
        $this->required = [];
        if ($nit === null) {
            $this->required[] = self::REQ_NIT;
        } elseif (!preg_match('/^\d{14}$/', $nit)) {
            $this->required[] = self::REQ_NIT_FORMAT;
        }
        return $this->required;
    }

    public function isValid(): bool {
        $this->valid = false;
        if (count($this->required) == 0) {
            $this->valid = true;
        } else {
            // Assuming $logger is an instance of LoggerInterface
            // If not, you can use error_log or any other logging mechanism
            $logger = new Logger(); // Replace with actual logger
            $logger->info("isValid(): " . count($this->required));
        }
        return $this->valid;
    }

    /**
     * @return array
     */
    public function getRequired(): array
    {
        return $this->required;
    }

    /**
     * @param array $required
     */
    public function setRequired(array $required): void
    {
        $this->required = $required;
    }

    /**
     * @return string
     */
    public function getValidNIT(): string
    {
        return $this->validNIT;
    }

    /**
     * @param string $validNIT
     */
    public function setValidNIT(string $validNIT): void
    {
        $this->validNIT = $validNIT;
    }
}
