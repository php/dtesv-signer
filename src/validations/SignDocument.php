<?php

namespace Datolab\DtesvSigner\validations;

use Datolab\DtesvSigner\filter\SignDocument as SignDocFilter;
use lib\Logger;

require_once 'Base.php';

class SignDocument extends Base
{
    public static Logger $logger; // Logger instance would be initialized in the constructor or another method in PHP

    public function v2validate(SignDocFilter $filter): array
    {
        $this->required = $this->validateNIT($filter->getNit());
        if($filter->getCompactSerialization() === null || strlen($filter->getCompactSerialization()) <= 0) {
            $this->required[] = Base::REQ_JWS;
        }
        return $this->required;
    }

    public function v1validate(SignDocFilter $filter): array
    {
        $this->required = $this->validateNIT($filter->getNit());
        if($filter->getDocumentName() === null || strlen($filter->getDocumentName()) <= 0) {
            $this->required[] = Base::REQ_DOCUMENT_NAME;
        }
        if($filter->getSignName() === null || strlen($filter->getSignName()) <= 0) {
            $this->required[] = Base::REQ_SIGN_NAME;
        }
        // Assuming logger is a PSR-3 compatible logger instance
        self::$logger->info("required: " . print_r($this->required, true));
        return $this->required;
    }

    public function v3validate(SignDocFilter $filter): array
    {
        $this->required = $this->validateNIT($filter->getNit());
        if($this->isValid() !== true) {
            $this->required[] = $this->validNIT;
        }

        if($filter->getPrivatePassword() === null || strlen($filter->getPrivatePassword()) <= 0) {
            $this->required[] = Base::REQ_PRIVATE_KEY;
        }
        return $this->required;
    }

    public function v5validate(SignDocFilter $filter): array
    {
        $this->required = $this->validateNIT($filter->getNit());
        if($filter->getDteJson() === null) {
            $this->required[] = Base::REQ_JSON_DTE;
        }
        if($filter->getPrivatePassword() === null || strlen($filter->getPrivatePassword()) <= 0) {
            $this->required[] = Base::REQ_PRIVATE_KEY;
        }
        return $this->required;
    }

//    public function getValidNIT(): string
//    {
//        return $this->validNIT;
//    }
}
