<?php

namespace Datolab\DtesvSigner\filter;

use Sabre\VObject\Parser\Json;

class SignDocument
{
    private string $publicPassword;
    private string $privatePassword; // required
    private string $nit; // required
    private string $documentName;
    private string $signName;
    private string $compactSerialization;
    private string $dteJson; // required
    private string $dte;
    private bool $active; // required

	public function toJson(): bool|string
	{
		return json_encode(
			[
				'nit' => $this->nit,
				'activo' => $this->active,
				'passwordPri' => $this->privatePassword,
				'dteJson' => $this->dteJson
			]
		);
	}
    public function getPublicPassword(): string {
        return $this->publicPassword;
    }

    public function setPublicPassword($publicPassword): void {
        $this->publicPassword = $publicPassword;
    }

    public function getPrivatePassword(): string {
        return $this->privatePassword;
    }

    public function setPrivatePassword($privatePassword): void {
        $this->privatePassword = $privatePassword;
    }

    public function getNit(): string {
        return $this->nit;
    }

    public function setNit($nit): void {
        $this->nit = $nit;
    }

    public function getActive(): bool {
        return $this->active;
    }

    public function setActive($active): void {
        $this->active = $active;
    }

    public function getDocumentName(): string {
        return $this->documentName;
    }

    public function setDocumentName($documentName): void {
        $this->documentName = $documentName;
    }

    public function getSignName(): string {
        return $this->signName;
    }

    public function setSignName($signName): void {
        $this->signName = $signName;
    }

    public function getCompactSerialization(): string {
        return $this->compactSerialization;
    }

    public function setCompactSerialization($compactSerialization): void {
        $this->compactSerialization = $compactSerialization;
    }

    public function getDteJson(): string {
        return $this->dteJson;
    }

    public function setDteJson($dteJson): void {
        $this->dteJson = $dteJson;
    }

    public function getDte(): string {
        return $this->dte;
    }

    public function setDte($dte): void {
        $this->dte = $dte;
    }
}
